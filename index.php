<?php

ini_set('display_errors', true);
require_once(__DIR__ . '/autoload.php');

/**
 * Uses
 */
use \Classes\Log\LogFactory;
use \Classes\Encode\EncodeFactory;

// Lets begin from logging
if (!$log = (new LogFactory())->getLogClass(LOG_CLASS)) {
    throw new Exception('Wrong logging class ' . LOG_CLASS);
}

// Now try to use encrypt */
if (!$encode = (new EncodeFactory())->getEncodeClass(ENCODE_CLASS)) {
    throw new Exception('Wrong encoding class ' . ENCODE_CLASS);
}

// Set log class to encrypt class
$encode->setLogClass($log);

// Create output string
if (!$output = $encode->encode(INPUT_STRING)) {
    throw new Exception('Something wrong with encode method');
}

// Create log for output
$log->create('Output string is "' . $output . '".');

// Get decode parameters
$decodeParams = $encode->getDecodeParams();

// Finish
die('finished');