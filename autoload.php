<?php

// Require base parameters
require_once(__DIR__ . '/config.php');

// Require logging classes
require_once(__DIR__ . '/Classes/Log/LogInterface.php');
require_once(__DIR__ . '/Classes/Log/LogFactory.php');
require_once(__DIR__ . '/Classes/Log/LogToDatabaseClass.php');
require_once(__DIR__ . '/Classes/Log/LogToDisplayClass.php');
require_once(__DIR__ . '/Classes/Log/LogToFileClass.php');
require_once(__DIR__ . '/Classes/Log/NoLoggingClass.php');
require_once(__DIR__ . '/Classes/Log/LogTrait.php');
require_once(__DIR__ . '/Classes/Log/LogTraitInterface.php');

// Require encoding classes
require_once(__DIR__ . '/Classes/Encode/EncodeInterface.php');
require_once(__DIR__ . '/Classes/Encode/EncodeFactory.php');
require_once(__DIR__ . '/Classes/Encode/EncodeClass.php');
require_once(__DIR__ . '/Classes/Encode/EncodeMd5Class.php');
require_once(__DIR__ . '/Classes/Encode/EncodeMd5Md5Class.php');
require_once(__DIR__ . '/Classes/Encode/EncodeSha1Class.php');
require_once(__DIR__ . '/Classes/Encode/EncodeSha1Md5Class.php');
require_once(__DIR__ . '/Classes/Encode/EncodeAes256cbcClass.php');