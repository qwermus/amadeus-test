<?php

namespace Classes\Encode;

/**
 * Interface EncodeInterface
 */
interface EncodeInterface
{
    /**
     * Encode a string
     *
     * @return string
     */
    public function encode(string $arg);

    /**
     * Get params to decode string
     *
     * @return string
     */
    public function getDecodeParams();
}