<?php

namespace Classes\Encode;

/**
 * Select class for encode
 */
class EncodeFactory
{
    /**
     * Check what class we will use
     *
     * @param $encodeClass
     */
    public function getEncodeClass(string $encodeClass)
    {
        // Check encode type
        switch ($encodeClass) {

            // Encode as md5
            case 'md5':
                return new EncodeMd5Class();
                // no break;

            // Encode as twice md5
            case 'md5md5':
                return new EncodeMd5Md5Class();
                // no break;

            // Encode as sha1
            case 'sha1':
                return new EncodeSha1Class();
                // no break;

            // Encode as md5 then sha1
            case 'sha1md5':
                return new EncodeSha1Md5Class();
            // no break;

            // Encode as aes-256-cbc
            case 'aes-256-cbc':
                return new EncodeAes256cbcClass();
            // no break;
        }

        return false;
    }
}