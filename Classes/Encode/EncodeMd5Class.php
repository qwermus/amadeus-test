<?php

namespace Classes\Encode;

/**
 * Class to encode as md5()
 */
class EncodeMd5Class extends EncodeClass
{
    /**
     * Encode a string
     *
     * @return string
     */
    public function encode(string $arg)
    {
        $this->log('input string for md5 encoding is "'.$arg.'"');
        return md5($arg);
    }
}