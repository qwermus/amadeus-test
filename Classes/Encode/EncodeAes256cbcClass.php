<?php

namespace Classes\Encode;

/**
 * Class to encode as aes-256-cbc
 */
class EncodeAes256cbcClass extends EncodeClass
{
    /**
     * Key to use for encripting
     * @var string
     */
    private $key;

    /**
     * Init vector
     * @var string
     */
    private $initVector;

    /**
     * Encrypt method
     *
     * @var string
     */
    private $method = 'aes-256-cbc';

    /**
     * Length of the key
     *
     * @var intval
     */
    private $keyLength = 32;

    /**
     * Options for encryption
     *
     * @var mixed
     */
    private $options = 0;

    /**
     * EncodeAes256cbcClass constructor.
     */
    public function __construct()
    {
        // Create key for encrypt and decrypt
        $this->key = openssl_random_pseudo_bytes($this->keyLength);

        // Create init vector for encrypt and decrypt
        $this->initVector = openssl_random_pseudo_bytes(openssl_cipher_iv_length($this->method));
    }

    /**
     * Encode a string
     *
     * @return string
     */
    public function encode(string $arg)
    {
        $this->log('input string for '.$this->method.' encoding is "'.$arg.'"');
        return openssl_encrypt($arg, $this->method, $this->key, $this->options, $this->initVector);
    }

    /**
     * Get params to decode string
     *
     * @return string
     */
    public function getDecodeParams()
    {
        // Data for decryption
        $params = [
            'method' => $this->method,
            'key' => base64_encode($this->key),
            'options' => $this->options,
            'initVector' => base64_encode($this->initVector),
        ];

        // Log params
        $this->log('params for decode are '.print_r($params, 1).'"');

        // Return them
        return $params;
    }
}