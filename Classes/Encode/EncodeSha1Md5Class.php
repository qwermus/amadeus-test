<?php

namespace Classes\Encode;

/**
 * Class to encode as md5(sha1())
 */
class EncodeSha1Md5Class extends EncodeClass
{
    /**
     * Encode a string
     *
     * @return string
     */
    public function encode(string $arg)
    {
        $this->log('input string for sha1-md5 encoding is "'.$arg.'"');
        return md5(sha1($arg));
    }
}