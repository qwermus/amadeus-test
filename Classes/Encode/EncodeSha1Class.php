<?php

namespace Classes\Encode;

/**
 * Class to encode as sha1()
 */
class EncodeSha1Class extends EncodeClass
{
    /**
     * Encode a string
     *
     * @return string
     */
    public function encode(string $arg)
    {
        $this->log('input string for sha1 encoding is "'.$arg.'"');
        return sha1($arg);
    }
}