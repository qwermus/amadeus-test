<?php

namespace Classes\Encode;

use \Classes\Log\LogTraitInterface;
use \Classes\Log\LogTrait;

/**
 * EncodeClass. Use common methods
 */
class EncodeClass implements EncodeInterface, LogTraitInterface
{
    use LogTrait;

    /**
     * Encode a string
     *
     * @return string
     */

    public function encode(string $arg)
    {
        return false;
    }

    /**
     * Get params to decode string or false
     *
     * @return string
     */
    public function getDecodeParams()
    {
        // Return false if there are no decode parameters
        return false;
    }
}