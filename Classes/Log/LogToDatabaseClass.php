<?php

namespace Classes\Log;

/**
 * Class Log To Database
 */
class LogToDatabaseClass implements LogInterface
{
    /**
     * @return bool
     */
    public function create(string $string)
    {
        // Here we save something to database
        return true;
    }
}