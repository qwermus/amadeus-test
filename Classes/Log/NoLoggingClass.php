<?php

namespace Classes\Log;

/**
 * Class Log To File
 */
class NoLoggingClass implements LogInterface
{
    /**
     * @return bool
     */
    public function create(string $string)
    {
        // Do nothing
        return true;
    }
}