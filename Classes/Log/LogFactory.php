<?php

namespace Classes\Log;

/**
 * Class select logging type
 */
class LogFactory
{
    /**
     * Check what class we will use
     *
     * @param $logClass
     */
    public function getLogClass(string $logClass)
    {
        // Check logging type
        switch ($logClass) {

            // Log to database
            case 'database':
                return new LogToDatabaseClass();
                // no break;

            // Log to database
            case 'file':
                return new LogToFileClass();
                // no break;

            // Log to display
            case 'display':
                return new LogToDisplayClass();
                // no break;

            // No logging
            case 'none':
                return new NoLoggingClass();
                // no break;
        }

        return false;
    }
}