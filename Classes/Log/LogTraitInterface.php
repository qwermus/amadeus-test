<?php

namespace Classes\Log;

/**
 * Interface LogTraitInterface
 * We can use it where we need to do a log
 */
interface LogTraitInterface
{
    /**
     * Set log class we will use
     *
     * @param $logClass
     * @return mixed
     */
    public function setLogClass(LogInterface $logClass);

    /**
     * Put something to log
     *
     * @param $string
     * @return bool
     */
    public function log(string $string);
}