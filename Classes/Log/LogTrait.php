<?php

namespace Classes\Log;

/**
 * Log trait.
 * Use it where u want to make a log
 */
trait LogTrait
{
    /**
     * Define log class
     *
     * @var
     */
    protected $log;

    /**
     * Set log class
     *
     * @param $log
     */
    public function setLogClass(LogInterface $logClass)
    {
        $this->log = $logClass;
    }

    /**
     * Put something into log
     */
    public function log(string $string)
    {
        $this->log->create($string);
    }
}