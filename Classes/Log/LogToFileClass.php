<?php

namespace Classes\Log;

/**
 * Class Log To File
 */
class LogToFileClass implements LogInterface
{
    /**
     * @return bool
     */
    public function create(string $string)
    {
        // Here we save something to file
        return true;
    }
}