<?php

namespace Classes\Log;

/**
 * Interface logInterface
 */
interface LogInterface
{
    /**
     * Create log record
     *
     * @return bool
     */
    public function create(string $string);
}