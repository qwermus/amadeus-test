<?php

namespace Classes\Log;

/**
 * Class Log To Display
 */
class LogToDisplayClass implements LogInterface
{
    /**
     * @return bool
     */
    public function create(string $string)
    {
        // Show message on display
        echo '<p>'.$string.'</p>';
        return true;
    }
}