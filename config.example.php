<?php

/**
 * Class of logging.
 * Available: database, file, display, none
 */
define('LOG_CLASS', 'display');

/**
 * This is an input string. Type everything you want
 */
define('INPUT_STRING', 'Some text here');

/**
 * Class for encoding
 * Available: md5, md5md5, sha1, sha1md5, aes-256-cbc
 */
define('ENCODE_CLASS', 'aes-256-cbc');